Kakoune Typewriter
==================

Do you miss the days of tapping out text on a typewriter,
watching the page slide slowly to the left as you typed,
then zinging back to the right for each new line?
Well, do I have the Kakoune plugin for you!

Installation
============

 1. Make the directory `~/.config/kak/autoload/`
    if it doesn't already exist.

        mkdir -p ~/.config/kak/autoload

 2. If it didn't already exist,
    create a symlink inside that directory
    pointing to Kakoune's default autoload directory
    so it Kakoune will still be able to find
    its default configuration.
    You can find Kakoune's runtime autoload directory
    by typing

        :echo %val{runtime}/autoload

    inside Kakoune.
 3. Put a copy of `typewriter.kak` inside
    the new autoload directory,
    such as by checking out this git repository:

        cd ~/.config/kak/autoload/
        git clone https://gitlab.com/Screwtapello/kakoune-typewriter.git

Usage
=====

Run `:typewriter-enable` to enable typewriter mode.

Run `:typewriter-disable` to turn it back off when you've had your fun.

Known Issues
============

Doesn't work with the wrap highlighter,
for obvious reasons.

The plugin doesn't force the cursor to the bottom line of the screen,
because there's no way to add padding at the top of the screen,
and because that would interfere with scrolling something fierce.
